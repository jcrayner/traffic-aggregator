package com.seek;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.seek.aggregators.ByDayTrafficAggregator;
import com.seek.aggregators.OverallTrafficAggregator;
import com.seek.aggregators.WindowedAggregator;
import com.seek.cli.Parser;
import com.seek.model.BoundedWindow;
import com.seek.model.TrafficAggregateRecord;
import com.seek.module.ApplicationModule;
import com.seek.processor.TrafficProcessor;
import com.seek.writer.ByDayTrafficWriter;
import com.seek.writer.OverallTrafficWriter;
import org.apache.commons.cli.CommandLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Traffic aggregation application.
 *
 * @author jeremyrayner
 */
public class Application {
  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

  private final Parser CLI;
  private final CsvMapper csvMapper;
  private final Set<TrafficProcessor<List<TrafficAggregateRecord>, ?>> processors;

  @Inject
  Application(Parser CLI, CsvMapper csvMapper, Set<TrafficProcessor<List<TrafficAggregateRecord>, ?>> processors) {
    this.CLI = CLI;
    this.csvMapper = csvMapper;
    this.processors = processors;
  }

  void run(String ... args) {
    try {
      CommandLine arguments = CLI.parse(args);

      File inputFile = Optional.ofNullable(arguments.getOptionValue("input-file"))
          .filter(input -> !input.isEmpty())
          .map(Paths::get)
          .map(Path::toFile)
          .filter(File::exists)
          .orElseThrow(() -> new IllegalArgumentException("Empty or invalid argument provided for input-file"));

      process(new BufferedInputStream(new FileInputStream(inputFile)), System.out);
    } catch (IOException ioe) {
      throw new RuntimeException(Throwables.getRootCause(ioe));
    } catch (Throwable e) {
      CLI.usage();

      Throwables.throwIfUnchecked(e);
      throw new RuntimeException(Throwables.getRootCause(e));
    }
  }

  private void process(final InputStream inputStream, final OutputStream outputStream) throws IOException {
    MappingIterator<TrafficAggregateRecord> recordIn = csvMapper.reader()
        .with(
            csvMapper.typedSchemaFor(TrafficAggregateRecord.class)
                .withoutHeader()
                .withColumnSeparator('\t')
                .withLineSeparator("\n")
        )
        .forType(TrafficAggregateRecord.class)
        .readValues(inputStream);

    List<TrafficAggregateRecord> records = ImmutableList.copyOf(recordIn);

    for (final TrafficProcessor<List<TrafficAggregateRecord>, ?> processor: processors) {
      processor.process(records, outputStream);
    }
  }

  public static void main(String[] args) {
    final Injector injector = Guice.createInjector(new ApplicationModule());
    Application application = injector.getInstance(Application.class);
    Runtime.getRuntime().addShutdownHook(new Thread(() -> LOGGER.info("Shutting down application")));

    application.run(args);
  }
}
