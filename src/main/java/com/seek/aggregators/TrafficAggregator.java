package com.seek.aggregators;

@FunctionalInterface
public interface TrafficAggregator<Aggregatable, AggregationResult> {
  AggregationResult aggregate(Aggregatable a);
}
