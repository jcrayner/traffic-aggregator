package com.seek.aggregators;

import com.google.inject.Inject;
import com.seek.model.BoundedWindow;
import com.seek.model.TrafficAggregateRecord;
import com.seek.window.DurationWindower;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


public class WindowedAggregator implements TrafficAggregator<List<TrafficAggregateRecord>, Map.Entry<BoundedWindow<LocalDateTime>, Integer>> {
  private final DurationWindower windower;
  private final TrafficAggregator<Map<BoundedWindow<LocalDateTime>, List<TrafficAggregateRecord>>, Map.Entry<BoundedWindow<LocalDateTime>, Integer>> innerAggregator;

  @Inject
  public WindowedAggregator(
      DurationWindower windower,
      TrafficAggregator<Map<BoundedWindow<LocalDateTime>, List<TrafficAggregateRecord>>, Map.Entry<BoundedWindow<LocalDateTime>, Integer>> innerAggregator
  ) {
    this.windower = windower;
    this.innerAggregator = innerAggregator;
  }

  @Override
  public Map.Entry<BoundedWindow<LocalDateTime>, Integer> aggregate(List<TrafficAggregateRecord> a) {
    return innerAggregator.aggregate(windower.window(a));
  }
}
