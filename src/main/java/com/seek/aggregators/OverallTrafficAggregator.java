package com.seek.aggregators;

import com.seek.model.TrafficAggregateRecord;

import java.util.List;
import java.util.stream.Stream;

public class OverallTrafficAggregator implements TrafficAggregator<List<TrafficAggregateRecord>, Integer> {
  public Integer aggregate(List<TrafficAggregateRecord> aggregatable) {
    return aggregatable.stream().mapToInt(TrafficAggregateRecord::getObservedTraffic).sum();
  }
}
