package com.seek.aggregators;

import com.seek.model.TrafficAggregateRecord;
import com.seek.model.TrafficDayAggregateRecord;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ByDayTrafficAggregator implements TrafficAggregator<List<TrafficAggregateRecord>, List<TrafficDayAggregateRecord>> {
  public List<TrafficDayAggregateRecord> aggregate(List<TrafficAggregateRecord> aggregatable) {
    return aggregatable.stream().collect(
        Collectors.groupingBy(
            (TrafficAggregateRecord record) -> record.getRecordTime().toLocalDate(),
            Collectors.summingInt(TrafficAggregateRecord::getObservedTraffic)
        )
       ).entrySet().stream()
        .map((entry) -> new TrafficDayAggregateRecord(entry.getKey(), entry.getValue()))
        .collect(Collectors.toList());
  }
}
