package com.seek.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDateTime;
import java.util.Objects;

@JsonPropertyOrder(value = { "recordTime", "observedTraffic" })
public class TrafficAggregateRecord {
  private final LocalDateTime recordTime;
  private final Integer observedTraffic;

  @JsonCreator
  public TrafficAggregateRecord(
      @JsonProperty("recordTime") LocalDateTime recordTime,
      @JsonProperty("observedTraffic") Integer observedTraffic
  ) {
    this.recordTime = recordTime;
    this.observedTraffic = observedTraffic;
  }

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
  public LocalDateTime getRecordTime() {
    return recordTime;
  }

  public Integer getObservedTraffic() {
    return observedTraffic;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TrafficAggregateRecord that = (TrafficAggregateRecord) o;
    return Objects.equals(recordTime, that.recordTime) &&
        Objects.equals(observedTraffic, that.observedTraffic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(recordTime, observedTraffic);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TrafficAggregateRecord{");
    sb.append("recordTime=").append(recordTime);
    sb.append(", observedTraffic=").append(observedTraffic);
    sb.append('}');
    return sb.toString();
  }
}
