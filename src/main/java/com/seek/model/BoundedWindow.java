package com.seek.model;

import java.util.Objects;

public class BoundedWindow<V> {
  private final V lowerBound;
  private final V upperBound;

  public BoundedWindow(V lowerBound, V upperBound) {
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
  }

  public V getLowerBound() {
    return lowerBound;
  }

  public V getUpperBound() {
    return upperBound;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BoundedWindow<?> that = (BoundedWindow<?>) o;
    return Objects.equals(lowerBound, that.lowerBound) &&
        Objects.equals(upperBound, that.upperBound);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lowerBound, upperBound);
  }

  @Override
  public String toString() {
    return lowerBound + " - " + upperBound;
  }
}
