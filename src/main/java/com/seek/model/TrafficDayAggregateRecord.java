package com.seek.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDate;
import java.util.Objects;

@JsonPropertyOrder(value = { "recordDate", "observedTraffic" })
public class TrafficDayAggregateRecord {
  private final LocalDate recordDate;
  private final Integer observedTraffic;

  @JsonCreator
  public TrafficDayAggregateRecord(
      @JsonProperty("recordDate") LocalDate recordDate,
      @JsonProperty("observedTraffic") Integer observedTraffic
  ) {
    this.recordDate = recordDate;
    this.observedTraffic = observedTraffic;
  }

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  public LocalDate getRecordDate() {
    return recordDate;
  }

  public Integer getObservedTraffic() {
    return observedTraffic;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TrafficDayAggregateRecord that = (TrafficDayAggregateRecord) o;
    return Objects.equals(recordDate, that.recordDate) &&
        Objects.equals(observedTraffic, that.observedTraffic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(recordDate, observedTraffic);
  }
}
