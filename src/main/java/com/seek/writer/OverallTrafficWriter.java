package com.seek.writer;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.text.StringSubstitutor;

import java.io.IOException;
import java.io.OutputStream;

public class OverallTrafficWriter implements TrafficWriter<Integer> {
  public void write(OutputStream outputStream, Integer value) throws IOException {
    outputStream.write(StringSubstitutor.replace("Overall traffic: ${value}", ImmutableMap.of("value", value)).getBytes(Charsets.UTF_8));
    outputStream.write(System.lineSeparator().getBytes(Charsets.UTF_8));
  }
}
