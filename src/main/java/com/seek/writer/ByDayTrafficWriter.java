package com.seek.writer;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Charsets;
import com.google.inject.Inject;
import com.seek.model.TrafficDayAggregateRecord;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class ByDayTrafficWriter implements TrafficWriter<List<TrafficDayAggregateRecord>> {
  private final CsvMapper csvMapper;

  @Inject
  public ByDayTrafficWriter(CsvMapper csvMapper) {
    this.csvMapper = csvMapper;
  }

  @Override
  public void write(OutputStream outputStream, List<TrafficDayAggregateRecord> value) throws IOException {
    outputStream.write(("Aggregated by day:" + System.lineSeparator()).getBytes(Charsets.UTF_8));

    csvMapper.writer()
        .with(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)
        .with(
            csvMapper.typedSchemaFor(TrafficDayAggregateRecord.class)
              .withoutHeader()
              .withColumnSeparator('\t')
              .withLineSeparator("\n")
        )
        .forType(TrafficDayAggregateRecord.class)
        .writeValues(outputStream)
        .writeAll(value);
  }
}
