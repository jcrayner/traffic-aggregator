package com.seek.writer;

import java.io.IOException;
import java.io.OutputStream;

@FunctionalInterface
public interface TrafficWriter<V> {
  void write(OutputStream outputStream, V value) throws IOException;
}
