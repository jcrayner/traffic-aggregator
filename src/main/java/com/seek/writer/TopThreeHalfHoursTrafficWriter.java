package com.seek.writer;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Charsets;
import com.seek.model.TrafficAggregateRecord;

import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Responsible for writing out the results of the top three hours filtration step. Could be generified further,
 * but trying not to get too pedantic.
 *
 * @author jeremyrayner
 */
public class TopThreeHalfHoursTrafficWriter implements TrafficWriter<List<TrafficAggregateRecord>> {
    private final CsvMapper csvMapper;

    @Inject
    public TopThreeHalfHoursTrafficWriter(CsvMapper csvMapper) {
        this.csvMapper = csvMapper;
    }

    @Override
    public void write(OutputStream outputStream, List<TrafficAggregateRecord> value) throws IOException {
        outputStream.write(("Top 3 half hours:" + System.lineSeparator()).getBytes(Charsets.UTF_8));
        csvMapper.writer()
                .with(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)
                .with(
                        csvMapper.typedSchemaFor(TrafficAggregateRecord.class)
                                .withoutHeader()
                                .withColumnSeparator('\t')
                                .withLineSeparator("\n")
                )
                .forType(TrafficAggregateRecord.class)
                .writeValues(outputStream)
                .writeAll(value);
    }
}
