package com.seek.window;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.seek.model.BoundedWindow;
import com.seek.model.TrafficAggregateRecord;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DurationWindower {
  private final Duration window;

  @Inject
  DurationWindower(Duration window) {
    this.window = window;
  }

  public Map<BoundedWindow<LocalDateTime>, List<TrafficAggregateRecord>> window(List<TrafficAggregateRecord> records) {
    ImmutableList.Builder<List<TrafficAggregateRecord>> builder = ImmutableList.builder();

    List<TrafficAggregateRecord> sorted = records.stream()
        .sorted(Comparator.comparing(TrafficAggregateRecord::getRecordTime))
        .collect(Collectors.toList());

    for(final TrafficAggregateRecord currentRecord: sorted) {
      //Duration.between().compareTo()
      int idx = sorted.indexOf(currentRecord);

      builder.add(
          // sublist the sorted list, with the current
          sorted.subList(idx, sorted.size()).stream()
              .filter((item) -> Duration.between(currentRecord.getRecordTime(), item.getRecordTime()).compareTo(window) < 0)
              .collect(Collectors.toList())
      );
    }

    return builder.build()
        .stream()
        .filter((group) -> group.size() == 3)
        .collect(
            Collectors.toMap(
                (group) -> new BoundedWindow<>(
                    group.stream().min(Comparator.comparing(TrafficAggregateRecord::getRecordTime))
                        .map(TrafficAggregateRecord::getRecordTime)
                        .orElseThrow(() -> new RuntimeException("no date found!")),
                    group.stream().max(Comparator.comparing(TrafficAggregateRecord::getRecordTime))
                        .map(TrafficAggregateRecord::getRecordTime)
                        .orElseThrow(() -> new RuntimeException("no date found!"))
                ),
                Function.identity(),
                (item1, item2) -> ImmutableList.<TrafficAggregateRecord>builder().addAll(item1).addAll(item2).build()
            )
        );
  }
}
