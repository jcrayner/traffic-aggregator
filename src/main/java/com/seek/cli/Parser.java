package com.seek.cli;

import com.google.inject.Inject;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Parser {
  private final CommandLineParser parser;
  private final Help help;
  private final Options options;

  @Inject
  Parser(CommandLineParser parser, Help help, Options options) {
    this.parser = parser;
    this.help = help;
    this.options = options;
  }

  public CommandLine parse(String ... args) throws ParseException {
    return parser.parse(options, args);
  }

  public void usage() {
    help.showHelp();
  }
}
