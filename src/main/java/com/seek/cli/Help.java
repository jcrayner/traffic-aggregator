package com.seek.cli;

import com.google.inject.Inject;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

public class Help {
  private final String appName;
  private final HelpFormatter formatter;
  private final Options options;

  @Inject
  Help(String appName, HelpFormatter formatter, Options options) {
    this.appName = appName;
    this.formatter = formatter;
    this.options = options;
  }

  public void showHelp() {
    formatter.printHelp("Seek CLI traffic aggregator", "", options, "", true);
  }
}
