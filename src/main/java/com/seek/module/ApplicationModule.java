package com.seek.module;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.seek.aggregators.ByDayTrafficAggregator;
import com.seek.aggregators.OverallTrafficAggregator;
import com.seek.aggregators.TrafficAggregator;
import com.seek.aggregators.WindowedAggregator;
import com.seek.model.BoundedWindow;
import com.seek.model.TrafficAggregateRecord;
import com.seek.model.TrafficDayAggregateRecord;
import com.seek.processor.TrafficProcessor;
import com.seek.window.DurationWindower;
import com.seek.writer.ByDayTrafficWriter;
import com.seek.writer.OverallTrafficWriter;
import com.seek.writer.TopThreeHalfHoursTrafficWriter;
import com.seek.writer.TrafficWriter;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.text.StringSubstitutor;

import javax.inject.Named;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Module binding and instance provisioning via Guice
 * @author jeremyrayner
 */
public class ApplicationModule extends AbstractModule {
  private static void configureMapper(ObjectMapper mapper) {
    mapper.registerModule(new GuavaModule());
    mapper.registerModule(new AfterburnerModule());
    mapper.registerModule(new ParameterNamesModule());
    mapper.registerModules(new Jdk8Module());
    mapper.registerModules(new JavaTimeModule());
  }

  @Provides
  public Set<TrafficProcessor<List<TrafficAggregateRecord>, ?>> processors(
      @Named("byDayProcessor") TrafficProcessor<List<TrafficAggregateRecord>, List<TrafficDayAggregateRecord>> byDayProcessor,
      @Named("overallProcessor") TrafficProcessor<List<TrafficAggregateRecord>, Integer> overallProcessor,
      @Named("topThreeHalfHoursProcessor") TrafficProcessor<List<TrafficAggregateRecord>, List<TrafficAggregateRecord>> topThreeHalfHoursProcessor,
      @Named("minWindowProcessor") TrafficProcessor<List<TrafficAggregateRecord>, Map.Entry<BoundedWindow<LocalDateTime>, Integer>> minWindowProcessor
   ) {
    return ImmutableSet.of(overallProcessor, byDayProcessor, topThreeHalfHoursProcessor, minWindowProcessor);
  }

  @Provides
  public CsvMapper csvMapper() {
    CsvMapper csvMapper = new CsvMapper();
    configureMapper(csvMapper);

    return csvMapper;
  }

  @Provides
  public Duration durationWindow() {
    return Duration.ofMinutes(90);
  }

  @Provides
  public Options cliOptions() {
    return new Options()
        .addOption(
            Option.builder()
                .longOpt("input-file")
                .numberOfArgs(1)
                .required()
                .build()
        );
  }

  @Provides
  @Named("minWriter")
  public TrafficWriter<Map.Entry<BoundedWindow<LocalDateTime>, Integer>> minTrafficWriter() {
    return (outputStream, value) -> {
      final BoundedWindow<LocalDateTime> window = value.getKey();
      final String writeOut = StringSubstitutor.replace(
          "Min period ${start} -> ${end}: ${value}",
          ImmutableMap.of(
              "start", window.getLowerBound(),
              "end", window.getUpperBound(),
              "value", value.getValue()
          )
      );

      outputStream.write(writeOut.getBytes(Charsets.UTF_8));
      outputStream.write(System.lineSeparator().getBytes(Charsets.UTF_8));
    };
  }

  @Provides
  @Named("byDayProcessor")
  public TrafficProcessor<List<TrafficAggregateRecord>, List<TrafficDayAggregateRecord>> dayProcessor(ByDayTrafficAggregator aggregator, ByDayTrafficWriter writer) {
    return new TrafficProcessor<>(aggregator, writer);
  }

  @Provides
  @Named("overallProcessor")
  public TrafficProcessor<List<TrafficAggregateRecord>, Integer> overallTrafficProcessor(OverallTrafficAggregator aggregator, OverallTrafficWriter writer) {
    return new TrafficProcessor<>(aggregator, writer);
  }

  @Provides
  @Named("minWindowProcessor")
  public TrafficProcessor<List<TrafficAggregateRecord>, Map.Entry<BoundedWindow<LocalDateTime>, Integer>> minWindowProcessor(
      @Named("minWindowAggregator") WindowedAggregator aggregator,
      @Named("minWriter") TrafficWriter<Map.Entry<BoundedWindow<LocalDateTime>, Integer>> writer
  ) {
    return new TrafficProcessor<>(aggregator, writer);
  }

  @Provides
  @Named("topThreeHalfHoursProcessor")
  public TrafficProcessor<List<TrafficAggregateRecord>, List<TrafficAggregateRecord>> topThreeHalfHoursProcessor(
          @Named("topThreeHalfHoursFilter") TrafficAggregator<List<TrafficAggregateRecord>, List<TrafficAggregateRecord>> filter,
          TopThreeHalfHoursTrafficWriter writer
  ) {
    return new TrafficProcessor<>(filter, writer);
  }

  @Provides
  @Named("topThreeHalfHoursFilter")
  public TrafficAggregator<List<TrafficAggregateRecord>, List<TrafficAggregateRecord>> topThreeHalfHoursFilter() {
    return (a) -> a.stream().sorted(Comparator.comparing(TrafficAggregateRecord::getObservedTraffic).reversed())
            .limit(3)
            .collect(Collectors.toList());
  }

  @Provides
  @Named("minWindowAggregator")
  WindowedAggregator minOfWindowAggregation(DurationWindower windower) {
    return new WindowedAggregator(
        windower,
        (aggregatable) -> aggregatable.entrySet().stream()
            .map(entry -> new AbstractMap.SimpleEntry<>(
                    entry.getKey(),
                    entry.getValue().stream()
                        .mapToInt(TrafficAggregateRecord::getObservedTraffic)
                        .sum()
                )
            )
            .min(Comparator.comparing(AbstractMap.SimpleEntry::getValue))
            .orElseThrow(() -> new RuntimeException("no values provided"))
    );
  }

  @Override
  protected void configure() {
    bind(CommandLineParser.class).to(DefaultParser.class);
  }
}
