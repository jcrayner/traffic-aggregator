package com.seek.processor;

import com.seek.aggregators.TrafficAggregator;
import com.seek.writer.TrafficWriter;

import java.io.IOException;
import java.io.OutputStream;

public class TrafficProcessor<Aggregatable, AggregationResult> {
  private final TrafficAggregator<Aggregatable, AggregationResult> aggregator;
  private final TrafficWriter<AggregationResult> writer;

  public TrafficProcessor(
      final TrafficAggregator<Aggregatable, AggregationResult> aggregator,
      final TrafficWriter<AggregationResult> writer
  ) {
    this.aggregator = aggregator;
    this.writer = writer;
  }

  public void process(Aggregatable values, OutputStream outputStream) throws IOException {
    writer.write(outputStream, aggregator.aggregate(values));
  }
}
