package com.seek;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.core.Is.is;

public class ApplicationTest {
  @Test
  public void runApplicationEndToEnd() throws Exception {
    final String expected = Resources.toString(Resources.getResource("fixtures/expected_output_1.txt"), Charsets.UTF_8);

    // maintain OG STDOUT
    final PrintStream stdout = System.out;

    try(final OutputStream stream = new ByteArrayOutputStream()) {
      final PrintStream output = new PrintStream(stream, true, Charsets.UTF_8.displayName());
      System.setOut(output);

      Application.main(new String[] { "--input-file=src/test/resources/fixtures/test_input_1.tsv" });
      assertThat(stream.toString(), is(equalToIgnoringWhiteSpace(expected)));
    } finally {
      System.setOut(stdout); // reset STDOUT
    }
  }
}