package com.seek.window;

import com.google.common.collect.ImmutableList;
import com.seek.model.BoundedWindow;
import com.seek.model.TrafficAggregateRecord;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class DurationWindowerTest {
  @Test
  @SuppressWarnings({"unchecked"})
  public void testThatWindowingProvidesANonEmptyResultSet() throws Exception {
    assertThat(
        new DurationWindower(Duration.ofMinutes(60))
            .window(
                ImmutableList.of(
                    new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T00:00:00"), 10), // will be ignored
                    new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T01:30:00"), 11),
                    new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T01:45:00"), 30),
                    new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T02:00:00"), 50),
                    new TrafficAggregateRecord(LocalDateTime.parse("2018-01-02T02:00:00"), 30) // will be ignored
                )
            ),
        Matchers.hasEntry(
            is(
                equalTo(
                    new BoundedWindow<>(
                        LocalDateTime.parse("2018-01-01T01:30:00"),
                        LocalDateTime.parse("2018-01-01T02:00:00")
                    )
                )
            ),
            IsIterableContainingInAnyOrder.containsInAnyOrder(
                is(equalTo(new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T01:30:00"), 11))),
                is(equalTo(new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T01:45:00"), 30))),
                is(equalTo(new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T02:00:00"), 50)))
            )
        )
    );
  }

  @Test
  public void testThatWindowingProvidesEmptyResult() throws Exception {
    final Map<BoundedWindow<LocalDateTime>, List<TrafficAggregateRecord>> actual = new DurationWindower(Duration.ofMinutes(1))
        .window(
            ImmutableList.of(
                new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T00:00:00"), 10), // will be ignored
                new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T01:30:00"), 11),
                new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T01:45:00"), 30),
                new TrafficAggregateRecord(LocalDateTime.parse("2018-01-01T02:00:00"), 50),
                new TrafficAggregateRecord(LocalDateTime.parse("2018-01-02T02:00:00"), 30) // will be ignored
            )
        );

    assertThat(actual.size(), is(equalTo(0)));
  }
}