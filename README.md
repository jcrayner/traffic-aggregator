Traffic Aggregator
=================

### Assumptions
1. All window based calculations are using a sliding window; values may appear within multiple windows as a result.
2. Window calculation outputs must contain 3 elements only (3 contiguous ```LocalDateTime``` instances)

### Testing Strategy
I've tended towards providing tests for implementations where the majority of the logic has been written by an engineer,
such as ```DurationWindower```, or application level tests where the intent is to validate the system as a whole, as per 
the expected output of the system.

### Running the Test Suite
All the tests can be executed via the gradle wrapper

```./gradlew tests``` OR ```gradlew.bat tests```

### Compiling the Application
This application provides both a standard `jar` and a `fat jar`.

Compile a standard `jar` without dependencies via gradle with `./gradlew jar`

Compile a `fat jar` with dependencies included via gradle with `./gradlew shadowJar` 

### Running the Application

Assuming a `fat jar` has been compiled, the output can be found within `build/libs`.

Execute the application via `java -jar build/libs/traffic-aggregator-1.0-SNAPSHOT-all.jar --input-file=path/to/input-file`